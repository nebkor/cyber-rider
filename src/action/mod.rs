use bevy::{
    diagnostic::FrameTimeDiagnosticsPlugin,
    ecs::reflect::ReflectResource,
    prelude::{App, IntoSystemConfigs, Plugin, Resource},
    reflect::Reflect,
};
use bevy_rapier3d::prelude::{NoUserData, RapierPhysicsPlugin};

mod components;
mod systems;

pub use components::*;
use systems::*;

#[derive(Resource, Default, Debug, Reflect)]
#[reflect(Resource)]
struct CyberLean {
    pub lean: f32,
}

pub struct CyberActionPlugin;
impl Plugin for CyberActionPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<MovementSettings>()
            .register_type::<MovementSettings>()
            .init_resource::<CatControllerSettings>()
            .init_resource::<ActionDebugInstant>()
            .init_resource::<CyberLean>()
            .register_type::<CyberLean>()
            .register_type::<CatControllerSettings>()
            .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
            .add_startup_system(timestep_setup)
            .add_plugin(FrameTimeDiagnosticsPlugin::default())
            .add_systems(
                (
                    gravity,
                    cyber_lean,
                    falling_cat,
                    input_forces,
                    drag,
                    tunnel_out,
                    surface_fix,
                )
                    .chain(),
            );
    }
}
