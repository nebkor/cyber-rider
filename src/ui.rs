use bevy::prelude::{
    AlignSelf, App, AssetServer, Color, Commands, Component, Plugin, Query, Res, Style, Text,
    TextBundle, TextSection, TextStyle, Transform, With,
};
#[cfg(feature = "inspector")]
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_rapier3d::prelude::Velocity;

use crate::bike::CyberBikeBody;

#[derive(Component)]
struct UpText;

fn setup_ui(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands
        .spawn(TextBundle {
            style: Style {
                align_self: AlignSelf::FlexEnd,
                ..Default::default()
            },
            // Use `Text` directly
            text: Text {
                // Construct a `Vec` of `TextSection`s
                sections: vec![TextSection {
                    value: "".to_string(),
                    style: TextStyle {
                        font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                        font_size: 40.0,
                        color: Color::GOLD,
                    },
                }],
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(UpText);
}

fn update_ui(
    state_query: Query<(&Velocity, &Transform), With<CyberBikeBody>>,
    mut text_query: Query<&mut Text, With<UpText>>,
) {
    let mut text = text_query.single_mut();
    let (velocity, xform) = state_query.single();
    let speed = velocity.linvel.dot(xform.forward());
    text.sections[0].value = format!("spd: {:.2}", speed);
}

pub struct CyberUIPlugin;

impl Plugin for CyberUIPlugin {
    fn build(&self, app: &mut App) {
        #[cfg(feature = "inspector")]
        app.add_plugin(WorldInspectorPlugin);

        app.add_startup_system(setup_ui).add_system(update_ui);
    }
}
