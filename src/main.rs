use bevy::prelude::*;
#[cfg(feature = "inspector")]
use cyber_rider::glamor::CyberGlamorPlugin;
use cyber_rider::{
    action::CyberActionPlugin, bike::CyberBikePlugin, camera::CyberCamPlugin, disable_mouse_trap,
    input::CyberInputPlugin, lights::CyberSpaceLightsPlugin, planet::CyberPlanetPlugin,
    ui::CyberUIPlugin,
};

//const CYBER_SKY: Color = Color::rgb(0.07, 0.001, 0.02);
const CYBER_SKY: Color = Color::rgb(0.64, 0.745, 0.937); // a light blue sky

fn main() {
    let mut app = App::new();
    app.insert_resource(Msaa::Sample4)
        .insert_resource(ClearColor(CYBER_SKY))
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                resolution: (2560.0, 1440.0).into(),
                ..Default::default()
            }),
            ..Default::default()
        }))
        .add_plugin(CyberPlanetPlugin)
        .add_plugin(CyberInputPlugin)
        .add_plugin(CyberActionPlugin)
        .add_plugin(CyberCamPlugin)
        .add_plugin(CyberSpaceLightsPlugin)
        .add_plugin(CyberUIPlugin)
        .add_plugin(CyberBikePlugin)
        .add_startup_system(disable_mouse_trap)
        .add_system(bevy::window::close_on_esc);

    #[cfg(feature = "inspector")]
    app.add_plugin(CyberGlamorPlugin);

    app.run();
}
