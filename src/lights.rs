use bevy::{pbr::CascadeShadowConfigBuilder, prelude::*};

use crate::planet::PLANET_RADIUS;

pub const LIGHT_RANGE: f32 = 90.0;

fn spawn_static_lights(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let pink_light = PointLight {
        intensity: 1_00.0,
        range: LIGHT_RANGE,
        color: Color::PINK,
        radius: 1.0,
        shadows_enabled: true,
        ..Default::default()
    };

    let blue_light = PointLight {
        intensity: 1_000.0,
        range: LIGHT_RANGE,
        color: Color::BLUE,
        radius: 1.0,
        shadows_enabled: true,
        ..Default::default()
    };

    commands.insert_resource(AmbientLight {
        color: Color::WHITE,
        brightness: 0.2,
    });

    let _cascade_shadow_config = CascadeShadowConfigBuilder {
        first_cascade_far_bound: 0.3,
        maximum_distance: 3.0,
        ..default()
    }
    .build();

    // up light
    commands
        .spawn(PointLightBundle {
            transform: Transform::from_xyz(0.0, PLANET_RADIUS + 30.0, 0.0),
            point_light: pink_light,
            ..Default::default()
        })
        .with_children(|builder| {
            builder.spawn(PbrBundle {
                mesh: meshes.add(
                    Mesh::try_from(shape::Icosphere {
                        radius: 10.0,
                        subdivisions: 2,
                    })
                    .unwrap(),
                ),
                material: materials.add(StandardMaterial {
                    base_color: Color::BLUE,
                    emissive: Color::PINK,
                    ..Default::default()
                }),
                ..Default::default()
            });
        });
    // down light
    commands
        .spawn(PointLightBundle {
            transform: Transform::from_xyz(0.0, -PLANET_RADIUS - 30.0, 0.0),
            point_light: blue_light,
            ..Default::default()
        })
        .with_children(|builder| {
            builder.spawn(PbrBundle {
                mesh: meshes.add(
                    Mesh::try_from(shape::Icosphere {
                        radius: 10.0,
                        subdivisions: 2,
                    })
                    .unwrap(),
                ),
                material: materials.add(StandardMaterial {
                    base_color: Color::PINK,
                    emissive: Color::BLUE,
                    ..Default::default()
                }),
                ..Default::default()
            });
        });
}

pub struct CyberSpaceLightsPlugin;
impl Plugin for CyberSpaceLightsPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn_static_lights);
    }
}
