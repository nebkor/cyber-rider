use bevy::prelude::{App, Color, Plugin};

// use crate::planet::CyberPlanet;

pub const BISEXY_COLOR: Color = Color::hsla(292.0, 0.9, 0.60, 1.1);

// public plugin
pub struct CyberGlamorPlugin;
impl Plugin for CyberGlamorPlugin {
    fn build(&self, app: &mut App) {
        {
            use bevy_rapier3d::render::{
                DebugRenderMode, DebugRenderStyle, RapierDebugRenderPlugin,
            };
            let style = DebugRenderStyle {
                multibody_joint_anchor_color: Color::GREEN.as_rgba_f32(),
                ..Default::default()
            };
            let mode = DebugRenderMode::CONTACTS
                | DebugRenderMode::SOLVER_CONTACTS
                | DebugRenderMode::JOINTS
                | DebugRenderMode::RIGID_BODY_AXES;

            let rplugin = RapierDebugRenderPlugin {
                style,
                always_on_top: true,
                enabled: true,
                mode,
            };

            app.add_plugin(rplugin);
        }
    }
}
