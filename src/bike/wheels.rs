use bevy::prelude::{shape::Torus as Tire, *};
use bevy_rapier3d::prelude::{
    Ccd, CoefficientCombineRule, Collider, ColliderMassProperties, CollisionGroups, Damping,
    ExternalForce, Friction, MultibodyJoint, PrismaticJointBuilder, Restitution,
    RevoluteJointBuilder, RigidBody, Sleeping, TransformInterpolation,
};

use super::{CyberSteering, CyberWheel, Meshterial, WheelConfig, BIKE_WHEEL_COLLISION_GROUP};

pub fn spawn_wheels(
    commands: &mut Commands,
    bike: Entity,
    conf: &WheelConfig,
    meshterials: &mut Meshterial,
) {
    let (membership, filter) = BIKE_WHEEL_COLLISION_GROUP;
    let wheels_collision_group = CollisionGroups::new(membership, filter);
    let wheel_y = conf.y;
    let stiffness = conf.stiffness;
    let not_sleeping = Sleeping::disabled();
    let ccd = Ccd { enabled: true };
    let limits = conf.limits;
    let (meshes, materials) = meshterials;
    let rake_vec: Vec3 = Vec3::new(0.0, 1.0, 0.57).normalize(); // about 30 degrees of rake

    let friction = Friction {
        coefficient: conf.friction,
        combine_rule: CoefficientCombineRule::Average,
    };

    let mut wheel_poses = Vec::with_capacity(2);

    // front
    {
        let wheel_x = 0.0;
        let wheel_z = -conf.front_forward;
        let offset = Vec3::new(wheel_x, wheel_y, wheel_z);
        wheel_poses.push((offset, Some(CyberSteering)));
    }

    // rear
    {
        let wheel_x = 0.0;
        let wheel_z = conf.rear_back;
        let offset = Vec3::new(wheel_x, wheel_y, wheel_z);
        wheel_poses.push((offset, None));
    }

    for (offset, steering) in wheel_poses {
        let (mesh, collider) = gen_tires(conf);

        let material = StandardMaterial {
            base_color: Color::Rgba {
                red: 0.01,
                green: 0.01,
                blue: 0.01,
                alpha: 1.0,
            },
            alpha_mode: AlphaMode::Opaque,
            perceptual_roughness: 0.5,
            ..Default::default()
        };

        let pbr_bundle = PbrBundle {
            material: materials.add(material),
            mesh: meshes.add(mesh),
            ..Default::default()
        };

        let mass_props = ColliderMassProperties::Density(conf.density);
        let suspension_damping = conf.damping;

        let suspension_axis = if steering.is_some() {
            rake_vec
        } else {
            Vec3::Y
        };

        let suspension_joint_builder = PrismaticJointBuilder::new(suspension_axis)
            .local_anchor1(offset)
            .limits(limits)
            .motor_position(limits[0], stiffness, suspension_damping);
        let suspension_joint = MultibodyJoint::new(bike, suspension_joint_builder);
        let fork_rb_entity = commands
            .spawn(RigidBody::Dynamic)
            .insert(suspension_joint)
            .insert(not_sleeping)
            .id();

        let axel_parent_entity = if let Some(steering) = steering {
            let neck_builder =
                RevoluteJointBuilder::new(rake_vec).local_anchor1(Vec3::new(0.0, 0.0, 0.1)); // this adds another 0.1m of trail
            let neck_joint = MultibodyJoint::new(fork_rb_entity, neck_builder);
            let neck = commands
                .spawn(RigidBody::Dynamic)
                .insert(neck_joint)
                .insert(steering)
                .insert(not_sleeping)
                .id();
            neck
        } else {
            fork_rb_entity
        };

        let axel_builder = RevoluteJointBuilder::new(Vec3::X);
        let axel_joint = MultibodyJoint::new(axel_parent_entity, axel_builder);
        let wheel_damping = Damping {
            linear_damping: 0.8,
            ..Default::default()
        };

        commands.spawn(pbr_bundle).insert((
            collider,
            mass_props,
            wheel_damping,
            ccd,
            not_sleeping,
            axel_joint,
            wheels_collision_group,
            friction,
            CyberWheel,
            ExternalForce::default(),
            Restitution::new(conf.restitution),
            SpatialBundle::default(),
            TransformInterpolation::default(),
            RigidBody::Dynamic,
        ));
    }
}

// do mesh shit
fn gen_tires(conf: &WheelConfig) -> (Mesh, Collider) {
    let wheel_rad = conf.radius;
    let tire_thickness = conf.thickness;
    let tire = Tire {
        radius: wheel_rad,
        ring_radius: tire_thickness,
        ..Default::default()
    };

    let mut mesh = Mesh::from(tire);
    let tire_verts = mesh
        .attribute(Mesh::ATTRIBUTE_POSITION)
        .unwrap()
        .as_float3()
        .unwrap()
        .iter()
        .map(|v| {
            //
            let v = Vec3::from_array(*v);
            let m = Mat3::from_rotation_z(90.0f32.to_radians());
            let p = m.mul_vec3(v);
            p.to_array()
        })
        .collect::<Vec<[f32; 3]>>();
    mesh.remove_attribute(Mesh::ATTRIBUTE_POSITION);
    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, tire_verts);

    let mut idxs = Vec::new();
    let indices = mesh.indices().unwrap().iter().collect::<Vec<_>>();
    for idx in indices.as_slice().chunks_exact(3) {
        idxs.push([idx[0] as u32, idx[1] as u32, idx[2] as u32]);
    }
    let wheel_collider = Collider::convex_decomposition(
        &mesh
            .attribute(Mesh::ATTRIBUTE_POSITION)
            .unwrap()
            .as_float3()
            .unwrap()
            .iter()
            .map(|v| Vec3::from_array(*v))
            .collect::<Vec<_>>(),
        &idxs,
    );

    (mesh, wheel_collider)
}
