use bevy::{
    prelude::{Component, ReflectResource, Resource},
    reflect::Reflect,
};

#[derive(Component)]
pub struct CyberBikeBody;

#[derive(Component)]
pub struct CyberSteering;

#[derive(Debug, Component)]
pub struct CyberWheel;

#[derive(Resource, Reflect)]
#[reflect(Resource)]
pub struct WheelConfig {
    pub front_forward: f32,
    pub rear_back: f32,
    pub y: f32,
    pub limits: [f32; 2],
    pub stiffness: f32,
    pub damping: f32,
    pub radius: f32,
    pub thickness: f32,
    pub friction: f32,
    pub restitution: f32,
    pub density: f32,
}

impl Default for WheelConfig {
    fn default() -> Self {
        Self {
            front_forward: 0.8,
            rear_back: 1.0,
            y: -0.1,
            limits: [-0.5, 0.1],
            stiffness: 190.0,
            damping: 8.0,
            radius: 0.25,
            thickness: 0.11,
            friction: 1.2,
            restitution: 0.95,
            density: 0.05,
        }
    }
}
