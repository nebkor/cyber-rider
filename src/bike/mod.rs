mod body;
mod components;
mod wheels;

use bevy::prelude::{
    App, Assets, IntoSystemConfig, Mesh, Plugin, ResMut, StandardMaterial, StartupSet,
};
use bevy_rapier3d::prelude::Group;

pub(crate) use self::components::*;
use self::{body::spawn_cyberbike, wheels::spawn_wheels};

pub const BIKE_BODY_COLLISION_GROUP: (Group, Group) = (Group::GROUP_1, Group::GROUP_1);
pub const BIKE_WHEEL_COLLISION_GROUP: (Group, Group) = (Group::GROUP_10, Group::GROUP_10);

type Meshterial<'a> = (
    ResMut<'a, Assets<Mesh>>,
    ResMut<'a, Assets<StandardMaterial>>,
);

pub struct CyberBikePlugin;
impl Plugin for CyberBikePlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(WheelConfig::default())
            .register_type::<WheelConfig>()
            .add_startup_system(spawn_cyberbike.in_base_set(StartupSet::PostStartup));
    }
}
