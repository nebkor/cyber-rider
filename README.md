# *Cyber Rider*

![I just love the deranged expression on this cyberbike's "face"](./cyber-rider-deranged.png "A third-person view of the cyberbike model on the cyberplanet, cybering.")

WIP.

All content is licensed under the [Peer Production License](./PPL.txt) or [Chaos License](./CHAOS.md), at users' choice.
